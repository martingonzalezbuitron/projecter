# Projecter

## Automagic creation of folder hierarchy for software projects in Linux

This file should be sourced for use. We recommend adding to ~/.bashrc:

```
source </path/to/projecter.sh>
```
