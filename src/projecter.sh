# Make project function

mkproject() {
: '
Purpose: Create a project folder with subfolders (scripts,data,doc,
         inputs,outputs). Use "-h" for help.
Preconditions: For project name do not use spaces.
               You need priviliges to create a folder in the directory
               where you want to create it.
Parameters: 1- project name                           #string
            2- directory where to create the project  #string
TODO: * Third parameter to add more folder/s.
      * Check if project name has "/" inside.
      * Add new functionality for projects using a new feature
        with a file like "config.ini" where all enviromental
	projects variables are activated when some command is
	executed inside that project. For example: when you what
	to refere to latest update path inside the tree project
	directory.
      * In addition of last functionality, this mkproject function
        should handle another and better flags arguments. For example:
	if you want to create another time branch with subfolders
	structure.
'
    #Cheking parameters
    if [[ "${#}" -ge 1 && "${#}" -le 2 && "${1}" != "--help" && "${1}" != "-help" && "${1}" != "-h" && "${1}" != "--h" ]]
    then
        #Creating project
        project_name="${1}"
        #Checking if exist the directory given
        if [[ -z "${2}" ]]
        then
            #Cheking if the name of the project exists.
            if [[ -n "${project_name}" ]]
            then
                #Cheking that the name is not a directory
                if [[ ! -d "${project_name}" ]]
                then
                    #Creting the folder structure
                    mkdir -p "${project_name}"/{src/scripts,data,processed_data,doc,results,outputs,tests}
                    touch "${project_name}"/{README.md,src/{LICENSE,requirements.txt}}
                    if [[ ! -s "${project_name}"/README.md ]]
                    then
                        printf "#<TITLE>\n\n##<SUBTITLE>\n\n###<3er-TITLE-LEVEL>\n\nWritten by Martín González Buitrón\nEmail: martingonzalezbuitron@gmail.com\n" > "${project_name}"/README.md
                    fi
                else
                    #Creating the structure of the project allthough the
                    #folder name already exists
                    echo "${project_name} is a directory and already exist."
                    echo "Building the structure of folders if need it ..."
                    mkdir -p "${project_name}"/{src/scripts,data,processed_data,doc,results,outputs,tests}
                    touch "${project_name}"/{README.md,src/{LICENSE,requirements.txt}}
                    if [[ ! -s "${project_name}"/README.md ]]
                    then
                        printf "#<TITLE>\n\n##<SUBTITLE>\n\n###<3er-TITLE-LEVEL>\n\nWritten by Martín González Buitrón\nEmail: martingonzalezbuitron@gmail.com\n" > "${project_name}"/README.md
                    fi
                    echo "Done !"
                    echo "Check yourself if everythings is Ok."
                fi
            else
                echo "Error: you must proportionate a project's name."
            fi
        else
            #Checking the last character in the project's name.
            if [[ "${2: -1}" == "/" ]]
            then
                project_dir="${2%/}"
            else
                project_dir="${2}"
            fi
            #Checking if project_dir is a directory.
            if [[ -d "${project_dir}" ]]
            then
                #Checking if project_name is not empty
                if [[ -n "${project_name}" ]]
                then
                    #Checking if "${project_dir}"/"${project_name}" is exist.
                    if [[ ! -d "${project_dir}/${project_name}" ]]
                    then
                        #Creting the folder structure
                        mkdir -p "${project_dir}/${project_name}"/{src/scripts,data,processed_data,doc,results,outputs,tests}
                        touch "${project_dir}/${project_name}"/{README.md,src/{LICENSE,requirements.txt}}
                        if [[ ! -s "${project_dir}/${project_name}"/README.md ]]
                        then
                            printf "#<TITLE>\n\n##<SUBTITLE>\n\n###<3er-TITLE-LEVEL>\n\nWritten by Martín González Buitrón\nEmail: martingonzalezbuitron@gmail.com\n" > "${project_dir}/${project_name}"/README.md
                        fi
                    else
                        #Creating the structure of the project allthough the
                        #folder name already exists.
                        echo "${project_name} is a directory and already exist."
                        echo "${project_dir}/${project_name}"
                        echo "Building the structure of folders if need it ..."
                        mkdir -p "${project_dir}/${project_name}"/{src/scripts,data,processed_data,doc,results,outputs,tests}
                        touch "${project_dir}/${project_name}"/{README.md,src/{LICENSE,requirements.txt}}
                        if [[ ! -s "${project_dir}/${project_name}"/README.md ]]
                        then
                            printf "#<TITLE>\n\n##<SUBTITLE>\n\n###<3er-TITLE-LEVEL>\n\nWritten by Martín González Buitrón\nEmail: martingonzalezbuitron@gmail.com\n" > "${project_dir}/${project_name}"/README.md
                        fi
                        echo "Done !"
                        echo "Check yourself if everythings is Ok."
                    fi
                else
                    echo "Error: you must proportionate a project's name."
                fi
            else
                echo "Error: your ${project_dir} is not a directory."
            fi
        fi
    #Checking the number or parameters
    elif [[ "${#}" -gt 2 ]]
    then
        echo "Error: Too many arguments."
        echo "Try with --help option"
    elif [[ "${#}" -eq 0 ]]
    then
        echo "Error: You need to put at least the project's name (without spaces)."
        echo "Try with --help option"
    #Printing help section
    elif [[ "${1}" == "--help" || "${1}" == "-help" || "${1}" == "-h" || "${1}" == "--h" ]]
    then
        echo "--------------------------------------------------------"
        echo "Usage:"
        echo "------"
        echo "       mkproject <PROJECT-NAME> <PROJECT-DIRECTORY>"
        echo
        echo
        echo "Purpose: Create a project folder with subfolders"
        echo "-------  (data,doc,results,outputs,processed_data,src,tests)."
        echo
        echo
        echo "Positional arguments:"
        echo "---------------------"
        echo
        echo "First argument  (\$1): PROJECT NAME           // string"
        echo "Second argument (\$2): PROJECT DIRECTORY      // string"
        echo
        echo
        echo "Optional:"
        echo "---------"
        echo
        echo "    --help"
        echo
        echo "--------------------------------------------------------"
    fi
}
